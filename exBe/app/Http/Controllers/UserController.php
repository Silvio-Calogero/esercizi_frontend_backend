<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;


class UserController extends Controller
{

    public function editUser(User $user){
        return view('user.editUser', compact('user'));
    }

    public function submitUser(Request $request, User $user){
        $user->name = $request->name;
        $user->email = $request->email;
        $user->age = $request->age;
        $user->phone_number = $request->phone_number;
        $user->address = $request->address;
        $user->city = $request->city;

        $user->save();

        return redirect('/');
    }
    
    public function listUser(User $user){
        $users = User::paginate(6);

        return view('user.listUser', compact('users'));
    }

}