<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;


class PublicController extends Controller
{
    public function welcome(User $user){
        return view('welcome', compact('user'));
    }
}
