<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PublicController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'welcome'])->name('welcome');

Route::get('/user/edit/{user}', [UserController::class, 'editUser'])->name('editUser');
Route::put('/user/submit/{user}', [UserController::class, 'submitUser'])->name('submitUser');
Route::get('/user/list/', [UserController::class, 'listUser'])->name('listUser');