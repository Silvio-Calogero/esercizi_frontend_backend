<x-layout>

    <div class="container text-dark text-center">
            <h1 class="py-3">LISTA UTENTI</h1>
    </div>

    <div class="container pt-3 mb-5">
        <div class="row">
            @foreach ($users as $user)
            <div class="col-12 col-md-4">
                <div class="card p-3 mt-2">
                    <p><b>NOME</b>: {{$user->name}}</p>
                    <p><b>EMAIL</b>: {{$user->email}}</p>
                    <p><b>ETA</b>: {{$user->age}}</p>
                    <p><b>NUMERO DI TELEFONO</b>: {{$user->phone_number}}</p>
                    <p><b>INDIRIZZO</b>: {{$user->address}}</p>
                    <p><b>CITTA</b>: {{$user->city}}</p>
                    <a href="{{route('editUser', compact('user'))}}" class="btn btn-warning">Modifica campi utente</a>
                </div>
            </div>
            @endforeach
            <div class="mt-3">
                {{$users->links()}}
            </div>
        </div>    
    </div>


</x-layout>