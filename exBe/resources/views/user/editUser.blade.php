<x-layout>


    <div class="container mb-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
            <h2 class="text-center text-primary my-4">REGISTRATI</h2>  
                <form method="POST" action="{{route('submitUser', compact('user'))}}">
                    @csrf
                    @method('PUT')
                    
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="list-unstyled">
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <div class="mb-3">
                            <label class="form-label">Nome</label>
                            <input type="text" class="form-control" name="name" value="{{$user->name}}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">E-Mail</label>
                        <input type="email" class="form-control" value="{{$user->email}}" name="email">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Età</label>
                        <input type="number" class="form-control" value="{{$user->age}}" name="age">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Numero di telefono</label>
                        <input type="text" class="form-control" value="{{$user->phone_number}}" name="phone_number">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Indirizzo</label>
                        <input type="text" class="form-control" value="{{$user->address}}" name="address">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Città</label>
                        <input type="text" class="form-control" value="{{$user->city}}" name="city">
                    </div>

                    <button type="submit" class="btn btn-primary" >Invia</button>
                </form>
            </div>
        </div>
    </div>
</x-layout>