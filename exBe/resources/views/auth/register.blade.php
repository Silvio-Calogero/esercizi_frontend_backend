<x-layout>

    <div class="container mb-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 userBox">
            <h2 class="text-center text-dark mt-1">REGISTRATI</h2>
                <form method="POST" action="{{route('register')}}">
                    @csrf

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="list-unstyled">
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <div class="mb-3">
                            <label class="form-label">Nome</label>
                            <input type="text" class="form-control" name="name">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">E-Mail</label>
                        <input type="email" class="form-control" placeholder="Inserisci E-Mail" name="email">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Password</label>
                        <input type="password" class="form-control" name="password">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Conferma Password</label>
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Età</label>
                        <input type="number" class="form-control" placeholder="Inserisci l'età" name="age">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Numero di telefono</label>
                        <input type="text" class="form-control" placeholder="" name="phone_number">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Indirizzo</label>
                        <input type="text" class="form-control" placeholder="" name="address">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Città</label>
                        <input type="text" class="form-control" placeholder="" name="city">
                    </div>

                    <button type="submit" class="btn btn-primary" >Invia</button>
                </form>
            </div>
        </div>
    </div>
</x-layout>