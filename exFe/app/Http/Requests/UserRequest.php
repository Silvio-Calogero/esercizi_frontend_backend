<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;


class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'surname' => 'required',
            'age' => 'required',
            'gender' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
            'cap' => 'required',
            'city' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Il campo Nome è obbligatorio',
            'surname.required' => 'Il campo Cognome è obbligatorio',
            'age.required' => 'Il campo Età è obbligatorio',
            'gender.required' => 'Il campo Genere è obbligatorio',
            'phone_number.required' => 'Il campo Numero di cellulare è obbligatorio',
            'address.required' => 'Il campo Indirizzo è obbligatorio',
            'city.required' => 'Il campo Città è obbligatorio',
        ];
    }
}
