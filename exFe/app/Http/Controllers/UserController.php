<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;


class UserController extends Controller
{

    public function listUser(User $user){
        $users = User::paginate(6);

        return view('user.listUser', compact('users'));
    }

}