<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Validation\Rule;
use App\Http\Requests\UserRequest;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Fortify\Http\Requests\RegisterRequest;


class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */

    protected $attribute;


    // 

    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'surname'  => ['required', 'string', 'max:255'],
            'email'=> [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
            'password' => $this->passwordRules(),
            'age'  => ['required', 'integer', 'max:255'],
            'gender'  => ['required', 'string', 'max:255'],
            'phone_number'  => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'cap' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
        ])->validate();

        return User::create([
            'name'=> $input['name'],
            'surname'=> $input['surname'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
            'age' => $input['age'],
            'gender' => $input['gender'],
            'phone_number' => $input['phone_number'],
            'address' => $input['address'],
            'cap' => $input['cap'],
            'city' => $input['city'],
        ]);
    }
}