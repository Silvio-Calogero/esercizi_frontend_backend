<x-layout>



    <div class="container mb-5">
        <div class="row justify-content-center">
            <h2 class="text-center text-success my-4">REGISTRATI</h2>
            <div class="col-12 col-md-9">
                <form method="POST" action="{{route('register')}}" id="formAnimation" class="userBox">
                    @csrf
                    

                    <!-- <div class="container mb-3">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div> -->

                    @if ($errors->any())                        
                        <div class="container alert alert-danger">
                            @error('name')
                                <li>Il campo nome è obbligatorio</li>
                            @enderror

                            @error('surname')
                                <li>Il campo cognome è obbligatorio</li>
                            @enderror

                            @error('email')
                                <li>L'E-Mail è obbligatoria</li>
                            @enderror

                            @error('password')
                                <li>La password è obbligatoria</li>
                            @enderror

                            @error('age')
                                <li>L'età è obbligatoria</li>
                            @enderror

                            @error('gender')
                                <li>Il genere è obbligatorio</li>
                            @enderror

                            @error('phone_number')
                                <li>Il numero di telefono è obbligatorio</li>
                            @enderror

                            @error('cap')
                                <li>Il cap è obbligatorio</li>
                            @enderror

                            @error('city')
                                <li>La città è obbligatoria</li>
                            @enderror
                        </div>
                    @endif

                    <div class="row mb-3">
                        <div class="col-12 col-md-6">
                            <label class="form-label">Nome</label>
                            <input type="text" class="form-control usersobj" name="name">
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label">Cognome</label>
                            <input type="text" class="form-control usersobj" name="surname">
                        </div>
                    </div>
                    <div class="col-12 col-md-3 mt-4 mb-3">
                                <label class="form-label">Genere</label>
                                    <select name="gender">
                                        <option value="Maschio">Maschio</option>
                                        <option value="Femmina">Femmina</option>
                                    </select>
                            </div>
                    <div class="row mb-3">
                            <div class="col-12 col-md-6">
                                <label class="form-label">Età</label>
                                <input type="number" class="form-control usersobj" name="age">
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label">Numero di telefono</label>
                                <input type="number" class="form-control usersobj" name="phone_number">
                            </div>
                        </div>

                    <div class="mb-3">
                        <label class="form-label">E-Mail</label>
                        <input type="email" class="form-control usersobj" name="email">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Password</label>
                        <input type="password" class="form-control usersobj" name="password">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Conferma Password</label>
                        <input type="password" class="form-control usersobj" name="password_confirmation">
                    </div>

                        <div class="row mb-3">
                            <div class="col-12 col-md-4">
                                <label class="form-label">Indirizzo</label>
                                <input type="text" class="form-control usersobj" name="address">
                            </div>
                            <div class="col-12 col-md-4">
                                <label class="form-label">CAP</label>
                                <input type="number" class="form-control usersobj" name="cap">
                            </div>
                            <div class="col-12 col-md-4">
                                <label class="form-label">Città</label>
                                <input type="text" class="form-control usersobj" name="city">
                            </div>
                        </div>
                        <div class="row justify-content-center text-center">
                            <div class="col-12 div col-md-4 py-3">
                                <button type="submit" id="pressButton" class="btn btn-success px-4">Invia</button>
                            </div>
                        </div>
                </form>
            </div>
                <div class="col-12 col-md-5">
                    <img src="img/working-from-home.png" class="img-fluid" alt="immagine-reg">
                </div>
        </div>
    </div>

</x-layout>