<x-layout>

    <div class="container mb-3">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>



    <div class="container mb-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
            <h2 class="text-center text-success my-4">ACCEDI</h2>    
                <form method="POST" action="{{route('login')}}" class="userBox">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label">E-Mail</label>
                        <input type="email" class="form-control" placeholder="Inserisci E-Mail" name="email">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Password</label>
                        <input type="password" class="form-control" placeholder="Inserisci Password" name="password">
                    </div>
                    <div class="row justify-content-center text-center">
                            <div class="col-12 div col-md-4 py-3">
                                <button type="submit" class="btn btn-success px-4">Invia</button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</x-layout>