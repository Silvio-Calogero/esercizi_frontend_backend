<x-layout>
    <div class="container-fluid welcPic">

        <div class="container text-success">
            <h1 class="headerTitle">BENVENUTO</h1>
        </div>

        @auth
        <div class="container text-center">
            <div class="row pt-5">
                <div class="col-4">
                    <div class="card">
                        <h2>{{Auth::user()->name}} {{Auth::user()->surname}}</h2>
                        <div">
                            <p class="fs-3">Hai {{Auth::user()->age}} anni</p>
                            <p><b>Numero di telefono: </b>{{Auth::user()->phone_number}}</p>
                            <p><b>Indirizzo: </b> {{Auth::user()->address}}, {{Auth::user()->cap}}, {{Auth::user()->city}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endauth

    </div>

</x-layout>