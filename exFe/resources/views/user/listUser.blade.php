<x-layout>

    <div class="container text-success text-center">
            <h1 class="py-3">LISTA UTENTI</h1>
    </div>

    <div class="container pt-3 mb-5">
        <div class="row">
            @foreach ($users as $user)
            <div class="col-12 col-md-4">
                <div class="card p-3 mt-2">
                    <p><b>NOME</b>: {{$user->name}}</p>
                    <p><b>COGNOME</b>: {{$user->surname}}</p>
                    <p><b>EMAIL</b>: {{$user->email}}</p>
                    <p><b>ETA</b>: {{$user->age}}</p>
                    <p><b>GENERE</b>: {{$user->gender}}</p>
                    <p><b>NUMERO DI TELEFONO</b>: {{$user->phone_number}}</p>
                    <p><b>INDIRIZZO</b>: {{$user->address}}</p>
                    <p><b>CAP</b>: {{$user->cap}}</p>
                    <p><b>CITTA</b>: {{$user->city}}</p>
                </div>
            </div>
            @endforeach
            <div class="mt-3">
                {{$users->links()}}
            </div>
        </div>    
    </div>


</x-layout>